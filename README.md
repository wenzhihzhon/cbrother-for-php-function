# cbrotherForPhpFunction

#### 介绍
使用CBrother封装php语言函数，方便调用

#### 安装教程

1. 进入cbrother 安装目录lib文件夹下
```cd D:\cbrother_v2.4.5_win_amd64\lib\```
2. 克隆项目
``` git clone https://gitee.com/wenzhihzhon/cbrother-for-php-function.git```
3. 修改项目名称为：cbrotherForPhpFunction

#### 使用说明

```
import lib/cbrotherForPhpFunction/php
function main(){
    var blankString  = "    1111    ";
    print php::trim(blankString);  //1111
}
```

#### 致谢
[CBrother官网](http://www.cbrother.net/main.html)